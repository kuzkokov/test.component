<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = [
    "NAME" => "Компонент для тестового задания",
    "DESCRIPTION" => "",
    "SORT" => 10,
    "CACHE_PATH" => "Y",
    "PATH" => [
        "ID" => "test",
        "NAME" => "Тест"
    ],
    "COMPLEX" => "N",
];