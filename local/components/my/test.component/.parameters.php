<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

CModule::IncludeModule("iblock");

$dbIBlock = CIBlock::GetList();
$arIblocks = [];
while ($arIBlock = $dbIBlock->Fetch()) {
    $arIblocks[$arIBlock["CODE"]] = "[" . $arIBlock["ID"] . "] " . $arIBlock["NAME"];
}

$arComponentParameters = [
    "GROUPS" => [],
    "PARAMETERS" => [
        "CACHE_TIME"  =>  [ "DEFAULT" => 3600],
        "MAIN_IBLOCK_TYPE" => [
            "PARENT" => "BASE",
            "NAME" => "Код основного инфоблока",
            "TYPE" => "LIST",
            "VALUES" => $arIblocks,
        ],
        "ADDITIONAL_IBLOCK_TYPE" => [
            "PARENT" => "BASE",
            "NAME" => "Код второго инфоблока",
            "TYPE" => "LIST",
            "VALUES" => $arIblocks,
        ],
        "PAGER_TEMPLATE" => [
            "PARENT" => "BASE",
            "NAME" => "Шаблон навигации",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ],
        "LIMIT" => [
            "PARENT" => "BASE",
            "NAME" => "Количество записей на странице",
            "TYPE" => "STRING",
            "DEFAULT" => "3",
        ]
    ]
];