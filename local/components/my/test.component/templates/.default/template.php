<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $arItem):?>
    <div class="item">
        <h2><?=$arItem['NAME']?></h2>
        <p class="item__change-date"><?=$arItem['TIMESTAMP_X']?></p>
        <p class="item__users">
            <? $strUsers = implode(', ', $arItem['USERS']);
            if ($strUsers !== ''): ?>
                <b>Пользователи:</b>
                <?= $strUsers ?>
            <? endif; ?>
        </p>
        <p class="item__additionals">
            <? if (count($arItem['ADDITIONALS'] > 0)): ?>
                [<?=implode(', ', $arItem['ADDITIONALS'])?>]
            <? endif; ?>
        </p>
    </div>
<? endforeach; ?>
<?=$arResult['NAV_STRING']?>
