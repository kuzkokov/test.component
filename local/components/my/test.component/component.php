<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader,
    Bitrix\Iblock;

if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 3600;
}

$arParams["MAIN_IBLOCK_TYPE"] = trim($arParams["MAIN_IBLOCK_TYPE"]);
$arParams["ADDITIONAL_IBLOCK_TYPE"] = trim($arParams["ADDITIONAL_IBLOCK_TYPE"]);

$arParams["LIMIT"] = (int)$arParams["LIMIT"];
if ($arParams["LIMIT"] <= 0) {
    $arParams["LIMIT"] = 3;
}

$arParams["PAGER_TEMPLATE"] = $arParams["PAGER_TEMPLATE"] ?: '.default';
if ($arParams["PAGER_TEMPLATE"]) {
    $arNavParams = [
        "nPageSize" => $arParams["LIMIT"]
    ];
    $arNavigation = CDBResult::GetNavParams($arNavParams);
} else {
    $arNavParams = [
        "nTopCount" => $arParams["LIMIT"]
    ];
    $arNavigation = false;
}

if ($this->startResultCache(false, [$GLOBALS['USER']->GetID(), $arNavigation])) {
    if (!Loader::includeModule("iblock")) {
        $this->abortResultCache();
        ShowError("Модуль Информационных блоков не установлен");
        return;
    }

    $rsIBlock = CIBlock::GetList([], [
        "ACTIVE" => "Y",
        "CODE" => [
            $arParams["MAIN_IBLOCK_TYPE"],
            $arParams["ADDITIONAL_IBLOCK_TYPE"]
        ],
        "SITE_ID" => SITE_ID,
    ]);

    if ($rsIBlock->SelectedRowsCount() !== 2) {
        $this->abortResultCache();
        Iblock\Component\Tools::process404("Раздел не найден");
        return;
    }

    $arSelect = [
        "ID",
        "IBLOCK_ID",
        "NAME",
        "TIMESTAMP_X",
        "PROPERTY_USERS",
        "PROPERTY_ADDITIONALS",
    ];

    $arFilter = [
        "IBLOCK_ID" => $arResult["ID"],
        "IBLOCK_LID" => SITE_ID,
        "ACTIVE" => "Y"
    ];

    $arSort = [
        "ID" => "DESC"
    ];

    $arResult["ITEMS"] = [];
    $rsElements = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, $arSelect);
    $arUsers = [];
    while($obElement = $rsElements->GetNextElement()) {
        $arItem = $obElement->GetFields();
        $arItem['USERS'] = [];
        foreach ($arItem['PROPERTY_USERS_VALUE'] as $user) {
            if (!$arUsers[$user]) {
                $arUser = CUser::GetByID($user)->Fetch();
                $arUsers[$user] = trim("{$arUser['LAST_NAME']} {$arUser['NAME']} {$arUser['SECOND_NAME']}");
            }
            $arItem['USERS'][] = $arUsers[$user];
        }

        $arItem['ADDITIONALS'] = $arItem['PROPERTY_ADDITIONALS_VALUE'];

        $arResult["ITEMS"][] = $arItem;
    }

    $arResult["NAV_STRING"] = $rsElements->GetPageNavStringEx(
        $navComponentObject,
        "Тест",
        $arParams["PAGER_TEMPLATE"],
        false,
        $this,
        []
    );
    $arResult["NAV_RESULT"] = $rsElement;

    $this->setResultCacheKeys([
        "ID",
        "ITEMS"
    ]);
    $this->includeComponentTemplate();
}